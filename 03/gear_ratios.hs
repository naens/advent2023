import Data.Char
import Text.ParserCombinators.ReadP
import Control.Applicative

extractResult :: [(a, String)] -> Maybe a
extractResult [] = Nothing
extractResult ((a,_):_) = Just a

getNumbers :: String -> [(Int, Int, Int)]
getNumbers s =
  let res = extractResult $ readP_to_S pairs s
  in case res of
    Just triples -> process_triples triples 0
    Nothing -> []
  where process_triples [] _ = []
        process_triples ((skip, number, numlen):t) index
          = (number, numlen, index+skip) : process_triples t (index+skip+numlen)

pair :: ReadP (Int, Int, Int)
pair = do
  ndots <- option 0 (fmap length (many1 (satisfy (not . isDigit))))
  numstr <- many1 (satisfy isDigit)
  tail <- look
  if length tail > 0 && isDigit (tail!!0) then pfail else return ()
  let numlen = length numstr
  let number = (read numstr)::Int
  return (ndots, number, numlen)

pairs :: ReadP [(Int, Int, Int)]
pairs = do
  res <- many1 pair
  dots <- option "" (many1 (satisfy (not . isDigit)))
  eof
  return res

getSymbols :: String -> [Int]
getSymbols s =
  let res = extractResult $ readP_to_S symbols s
  in case res of
    Just ddd -> get_symbols_rec ddd 0
    Nothing -> []
  where get_symbols_rec [] _ = []
        get_symbols_rec (h:t) i = (h+i):(get_symbols_rec t (i+1+h))

is_symbol :: Char -> Bool
is_symbol x = not (isDigit x) && not (x == '.')

symbols :: ReadP [Int]
symbols = do
  res <- many1 symbol
  _ <- option "" (many1 (satisfy (not . is_symbol)))
  eof
  return res
symbol :: ReadP Int
symbol = do
  ndots <- option 0 (fmap length (many1 (satisfy (not . is_symbol))))
  c <- satisfy is_symbol
  return ndots

subList :: [a] -> Int -> Int -> [a]
subList [] _ _ = []
subList _ _ 0 = []
subList (h:t) 0 to = h : subList t 0 (to-1)
subList (h:t) from to = subList t (from-1) (to-1)

hasSymbolsAt :: [[Int]] -> (Int, Int) -> (Int, Int) -> Bool
hasSymbolsAt array (xfrom, yfrom) (xto, yto) =
  matchSubRange array yfrom yto (\row -> (length (filter (\x -> x >= xfrom && x < xto) row) > 0))

matchSubRange :: [a] -> Int -> Int -> (a -> Bool) -> Bool
matchSubRange [] _ _ _ = False
matchSubRange _ _ 0 _ = False
matchSubRange (h:t) 0 to fun
  | fun h == True = True
  | otherwise = matchSubRange t 0 (to-1) fun
matchSubRange (_:t) from to fun = matchSubRange t (from-1) (to-1) fun

hasSymbol :: (Int, Int, Int, Int) -> [[Int]] -> Bool
hasSymbol (n, len, x, y) symbol_array =
  hasSymbolsAt symbol_array (max (x-1) 0, max (y-1) 0) (x+len+1, y+2)

number_list :: [[(Int, Int, Int)]] -> [(Int, Int, Int, Int)]
number_list naa = number_list_rec naa 0
number_list_rec :: [[(Int, Int, Int)]] -> Int -> [(Int, Int, Int, Int)]
number_list_rec [] _ = []
number_list_rec (h:t) row =
  let ra = map (\(n,l,x) -> (n, l, x, row)) h
  in ra ++ (number_list_rec t (row+1))

getGears :: [String] ->[(Int, Int)]
getGears lines = gears_rec lines 0
gears_rec :: [String] -> Int -> [(Int, Int)]
gears_rec [] _ = []
gears_rec (line:lines) y =
  (map (\x -> (x, y)) (line_gears line)) ++ (gears_rec lines (y + 1))
line_gears :: String -> [Int]
line_gears line = line_gears_rec line 0
line_gears_rec :: String -> Int -> [Int]
line_gears_rec [] _ = []
line_gears_rec (c:t) x
  | c == '*' = x:(line_gears_rec t (x+1))
  | otherwise = line_gears_rec t (x+1)

numbers_to_gears numbers gear_x gear_y =
  let set1 = filter (\(n,l,x,y) -> x >= gear_x-l && x <= gear_x+1 && y == gear_y - 1) numbers
      set2 = filter (\(n,l,x,y) -> x >= gear_x-l && x <= gear_x+1 && y == gear_y) numbers
      set3 = filter (\(n,l,x,y) -> x >= gear_x-l && x <= gear_x+1 && y == gear_y + 1) numbers
  in map (\(n,l,x,y) -> n) (set1 ++ set2 ++ set3)

main :: IO ()
main = do
  contents <- lines <$> readFile "input.txt"
  let numbers = number_list (map getNumbers contents)
  let symbols = map getSymbols contents
  let gears = getGears contents
  let filtered_numbers = filter (\x -> hasSymbol x symbols) numbers
  let sum_filtered = sum $ map (\(n,_,_,_) -> n) filtered_numbers
  putStr "sum_filtered="
  putStrLn (show sum_filtered)
  putStr "gears="
  putStrLn (show gears)
  let gear_numbers = map (\(x,y) -> numbers_to_gears numbers x y) gears
  putStr "gear_numbers="
  putStrLn (show gear_numbers)
  let result = sum $ map (\x -> product x) (filter (\x -> length x == 2) gear_numbers)
  putStrLn (show result)
