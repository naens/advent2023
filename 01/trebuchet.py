#! /usr/bin/python

def find_digit(line, start, step):
    if start < 0 or start >= len(line) or step == 0:
        return -1
    if line[start:].startswith("one") or line[start:].startswith("1"):
        return 1
    if line[start:].startswith("two") or line[start:].startswith("2"):
        return 2
    if line[start:].startswith("three") or line[start:].startswith("3"):
        return 3
    if line[start:].startswith("four") or line[start:].startswith("4"):
        return 4
    if line[start:].startswith("five") or line[start:].startswith("5"):
        return 5
    if line[start:].startswith("six") or line[start:].startswith("6"):
        return 6
    if line[start:].startswith("seven") or line[start:].startswith("7"):
        return 7
    if line[start:].startswith("eight") or line[start:].startswith("8"):
        return 8
    if line[start:].startswith("nine") or line[start:].startswith("9"):
        return 9
    if line[start:].startswith("zero") or line[start:].startswith("0"):
        return 0
    return find_digit(line, start+step, step)



file1 = open('input.txt', 'r')
Lines = file1.readlines()

s = 0
numl = 1
for line in Lines:
    fst = find_digit(line, 0, 1)
    lst = find_digit(line, len(line)-1, -1)
    print("[{}] {}{}".format(numl, fst, lst))
    s += (fst*10 + lst)
    numl += 1
print(s)
