import System.IO

import Text.Printf
import Data.Char
import Data.Maybe
import Control.Monad

starts_with :: String -> String -> Bool
starts_with _ [] = True
starts_with [] _ = False
starts_with (h1:t1) (h2:t2)
  | h1 == h2 = starts_with t1 t2
  | otherwise = False

digits = [("one", 1), ("two", 2), ("three", 3), ("four", 4), ("five", 5),
          ("six", 6), ("seven", 7), ("eight", 8), ("nine", 9), ("zero", 0),
          ("1", 1), ("2", 2), ("3", 3), ("4", 4), ("5", 5), ("6", 6),
          ("7", 7), ("8", 8), ("9", 9), ("0", 0)]

h1 :: String -> Direction -> (String, Int) -> Maybe Int
h1 str Forward (s, n) = if starts_with str s then Just n else Nothing
h1 str Backwards (s, n) = if starts_with str (reverse s) then Just n else Nothing


data Direction = Forward | Backwards

match_digit :: String -> Direction -> Maybe Int
match_digit str direction =
  let l = catMaybes (map (h1 str direction) digits)
  in if length l > 0  then Just (sum l) else Nothing

find_digit :: String -> Direction -> Maybe Int
find_digit [] _ = Nothing
find_digit s@(h:t) direction =
  let d = match_digit s direction
  in case d of
    Just n -> Just n
    Nothing -> find_digit t direction

first_digit :: String -> Maybe Int
first_digit str = find_digit str Forward

last_digit :: String -> Maybe Int
last_digit str = find_digit (reverse str) Backwards

calibrate :: String -> Int
calibrate s =
  let f = first_digit s
      l = last_digit s
  in case (f, l) of
    (Just fst, Just lst) ->  fst * 10 + lst
    _ -> 0


-- process_document :: [String] -> Int
process_document s = sum $ map calibrate s

main = do
  content <- readFile "input.txt"
  let ln = lines content
  let cs = map calibrate ln
  printf "<<%s>>\n" (show (process_document ln))
  --printf (show cs)
  
