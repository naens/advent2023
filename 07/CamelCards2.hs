import Data.Char
import Data.List

data Card = Card Char deriving (Eq, Show)
instance Ord Card where
  compare card1 card2 = compare (cardValue card1) (cardValue card2)

cardValue :: Card -> Int
cardValue (Card 'J') = 1
cardValue (Card 'T') = 10
cardValue (Card 'Q') = 11
cardValue (Card 'K') = 12
cardValue (Card 'A') = 13
cardValue (Card c) = digitToInt c

data HandType = High | OnePair | TwoPair | Three | Full | Four | Five
  deriving (Show, Eq)

instance Ord HandType where
  compare ht1 ht2 = compare (handTypeValue ht1) (handTypeValue ht2)
    where handTypeValue High = 1
          handTypeValue OnePair = 2
          handTypeValue TwoPair = 3
          handTypeValue Three = 4
          handTypeValue Full = 5
          handTypeValue Four = 6
          handTypeValue Five = 7
        
data Hand = Hand [Card] deriving Eq
instance Show Hand where
  show (Hand cards) =
    let cardChars = map (\(Card c) -> c) cards
    in "Hand:" ++ cardChars
instance Ord Hand where
  compare hand1@(Hand cards1) hand2@(Hand cards2) =
    let type1 = handType hand1
        type2 = handType hand2
    in if type1 == type2
       then compare cards1 cards2
       else compare type1 type2


charToCard :: Char -> Card
charToCard c
  | isDigit c && (digitToInt c) >= 2 = Card c
  | c == 'T' || c == 'J' || c == 'Q' || c == 'K' || c == 'A' = Card c

readHand :: String -> Hand
readHand = Hand . map charToCard
readLine :: String -> (Hand, Int)
readLine line =
  let (cardString, bidString) = split line
  in (readHand cardString, read bidString)
  where split s =
          let (s1, s2) = break (==' ') s
          in (s1, drop 1 s2)

-- amount of repetiton (ex, aabccc -> 213, sorted: 123)
handProfile :: Hand -> [Int]
handProfile (Hand cards) =
  let jokers = length $ filter (== (Card 'J')) cards
  in case jokers of
    0 -> sort $ getProfile $ sort cards
    _ -> incLast (sort $ getProfile $ sort (filter (/= (Card 'J')) cards)) jokers
      where incLast :: [Int] -> Int-> [Int]
            incLast l n =
              case reverse l of
                h:t -> reverse ((h+n):t)
                [] -> [5]
    


getProfile :: Eq a => [a] -> [Int]
getProfile [] = []
getProfile (h:t) = getProfileRec t h [] 1
  where getProfileRec :: Eq a =>  [a] -> a -> [Int] -> Int -> [Int]
        getProfileRec [] _ a n = n:a
        getProfileRec (h:t) c a n
          | h == c = getProfileRec t h a (n+1)
          | otherwise = getProfileRec t h (n:a) 1
  

handType :: Hand -> HandType
handType hand =
  let profile = handProfile hand
  in case profile of
    [1,1,1,1,1] -> High
    [1,1,1,2] -> OnePair
    [1,2,2] -> TwoPair
    [1,1,3] -> Three
    [2,3] -> Full
    [1,4] -> Four
    [5] -> Five

main :: IO ()
main = do
  file_str <- readFile "input.txt"
  let contents = map readLine (lines file_str)
  let hands = map fst contents
  let types = map handType hands
  let sorted_data = sort contents
  let zipped_data = zip [1..] sorted_data
  let winnings = map (\(rank, (_, bid)) -> rank * bid) zipped_data
  putStrLn (show zipped_data)
  putStrLn (show winnings)
  putStrLn (show (sum winnings))
