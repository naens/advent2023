import Text.ParserCombinators.ReadP
import Data.Char
import Data.List
import Data.Maybe
import Data.Bits

data Card = Card {card_id :: Int, winning :: [Int], numbers :: [Int]} deriving Show


num :: ReadP Int
num = do
  char ' '
  skipSpaces
  number <- fmap read (many1 (satisfy isDigit))
  return number

parseCard :: ReadP Card
parseCard = do
  string "Card"
  card_id <- num
  char ':'
  wn <- many1 num
  skipSpaces
  char '|'
  nn <- many1 num
  skipSpaces
  eof
  return (Card card_id (sort wn) (sort nn))

readCards :: [String] -> [Card]
readCards cards = catMaybes (map (\card -> readCard card) cards)

readCard :: String -> Maybe Card
readCard s =
  let res = readP_to_S parseCard s
  in if length res > 0 then
       Just (fst (res !! 0))
     else
       Nothing

cardWorth :: Card -> Int
cardWorth c =
  let common = intersect (winning c) (numbers c)
  in if common == [] then 0 else shiftL 1 ((length common) - 1)

question2 cards = sum $ map (\(a,b,c) -> b)  (q2_rec (map (\c -> (c, 1)) cards))
q2_rec [] = []
q2_rec ((c,n):t) =
  let m = length $ intersect (winning c) (numbers c)
      t2 = update_cards m n t
  in (card_id c, n, m):(q2_rec t2)
  where update_cards 0 _ t = t
        update_cards m n ((c,n0):t) = (c,n0+n):(update_cards (m-1) n t)

main :: IO ()
main = do
  contents <- readFile "input.txt"
  let cards = readCards $ lines contents
  let counts = map cardWorth cards
  putStrLn (show (sum counts)) -- question 1
  let q2res = question2 cards
  putStrLn (show q2res)
