import Data.Char

data Info = Info {game_id :: Int, red :: Int, green :: Int, blue :: Int} deriving Show
data Color = Red | Green | Blue deriving (Show, Eq)

skip_spaces :: String -> String
skip_spaces [] = []
skip_spaces s@(h:t)
  | h == ' ' = t
  | otherwise = s

read_string :: String -> String -> Maybe String
read_string [] input = Just input
read_string _ [] = Nothing
read_string str@(str_h:str_t) (h:t)
  | str_h == h = read_string str_t t
  | otherwise = Nothing

read_number :: String -> Maybe (Int, String)
read_number [] = Nothing
read_number s@(h:t) =
  if isDigit h then Just (read_number_rec s 0)
  else Nothing
read_number_rec s@(h:t) a
          | isDigit h = read_number_rec t (a*10 + (digitToInt h))
          | otherwise = (a, s)
          
read_game_header :: String -> (Int, String)
read_game_header s0 =
   let maybe_tail1 = read_string "Game" s0
   in case maybe_tail1 of
     Just s1 -> let maybe_id = read_number (skip_spaces s1)
                in case maybe_id of
                     Just (game_id, s2) -> (game_id, s2)
                     Nothing -> error "syntax error in read_game_header"
     Nothing -> error "syntax error in read_game_header"


read_color :: String -> Maybe (Color, String) 
read_color s0 =
  let maybe_s1 = read_string "red" (skip_spaces s0)
  in case maybe_s1 of
       Just s1 -> Just (Red, s1)
       Nothing -> let maybe_s2 = read_string "green" (skip_spaces s0)
          in case maybe_s2 of
               Just s2 -> Just (Green, s2)
               Nothing -> let maybe_s3 = read_string "blue" (skip_spaces s0)
                 in case maybe_s3 of
                      Just s3 -> Just (Blue, s3)
                      Nothing -> Nothing

read_record_to_list :: String -> ([(Color, Int)], String)
read_record_to_list s0 = read_record_rec s0 []

read_record_rec :: String -> [(Color, Int)] -> ([(Color, Int)], String)
read_record_rec s0 acc =
  let maybe_number = read_number (skip_spaces s0)
  in case maybe_number of
    Just (number, s1) ->
       let maybe_color = read_color (skip_spaces s1)
       in case maybe_color of
         Just (color, s2) ->
           let maybe_tail = read_string "," (skip_spaces s2)
           in case maybe_tail of
                Just s3 -> read_record_rec s3 ((color, number):acc)
                Nothing -> ((color, number):acc, s2) -- no more comma, nothing left
         Nothing -> error "syntax error in read_record"
    Nothing -> (acc, s0)

read_game_record :: String -> ((Int, Int, Int), String)
read_game_record s0 =
  let (record_list, s1) = read_record_to_list s0
  in (sum_rec record_list (0, 0, 0), s1)
  where sum_rec [] acc = acc
        sum_rec ((Red, n):t) (red, green, blue) = sum_rec t (red+n, green, blue)
        sum_rec ((Green, n):t) (red, green, blue) = sum_rec t (red, green+n, blue)
        sum_rec ((Blue, n):t) (red, green, blue) = sum_rec t (red, green, blue+n)

read_game_body :: String  -> [(Int, Int, Int)]
read_game_body s = read_game_body_rec s []
  where read_game_body_rec [] acc = acc
        read_game_body_rec s0 acc =
          let (c1, s1) = read_game_record s0
          in let maybe_tail2 = read_string ";" s1
             in case maybe_tail2 of
                  Just s2 -> let d = read_game_body s2
                             in c1:d
                  Nothing -> [c1]

read_game_max :: String -> (Int, Int, Int)
read_game_max s =
  let lst = read_game_body s
  in max_lst_rec lst (0,0,0)
  where max_lst_rec :: [(Int, Int, Int)] -> (Int, Int, Int) -> (Int, Int, Int)
        max_lst_rec [] a = a
        max_lst_rec ((r0, g0, b0):t) (r1, g1, b1) = max_lst_rec t (max r0 r1, max g0 g1, max b0 b1)

read_game_line :: String -> Info
read_game_line s0 =
  let (game_id, s1) = read_game_header s0
  in let maybe_tail2 = read_string ":" (skip_spaces s1)
     in case maybe_tail2 of
          Just s2 -> let (red, green, blue) = read_game_max s2
                     in Info game_id red green blue
          Nothing -> error "syntax error in read_game_line"

game_filter :: Info -> Bool
game_filter (Info id red green blue) = red <= 12 && green <= 13 && blue <= 14

question1 game_lines = 
  sum $ map (\game -> (game_id game)) $ filter game_filter game_lines

question2 game_lines = sum $ map (\g -> (red g) * (green g) * (blue g)) game_lines

main = do
  contents <- readFile "input.txt"
  let ls = lines contents
  let game_lines = map read_game_line ls
  putStrLn (show (question2 game_lines))
